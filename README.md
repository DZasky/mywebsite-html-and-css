# My First Website

My final project of Frontend Bootcamp, module 1: HTML5 & CSS3. That basic knowledge of frontend development let me to finalize this project with use:

- HTML5 and CSS3
- RWD (Fluid layout, @media, mobile-first)
- good practices